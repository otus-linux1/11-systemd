# Systemd

Для запуска виртаульной машины использовался образ (бокс) с Centos 8

## cron task

> Написать service, который будет раз в 30 секунд мониторить лог
на предмет наличия ключевого слова (файл лога и ключевое слово должны задаваться в /etc/sysconfig)

примитивный скрипт monitor.sh лежит в корне проекта, юнитфайлы в `etc/systemd/system`.

## spawn-fcgi

> Из репозитория epel установить spawn-fcgi и переписать init-скрипт на unit-файл (имя service должно называться так же: spawn-fcgi)

см. `spawn-fcgi.service` в `etc/systemd/system`

## httpd

> Дополнить unit-файл httpd (он же apache) возможностью запустить несколько инстансов сервера с разными конфигурационными файлами

В используем образе ОС в каталоге `/usr/lib/systemd/system/`
уже присутствует файл `httpd@.service` следующего содержания:

<details>
<summary>httpd@.service</summary>

```
[Unit]
Description=The Apache HTTP Server
After=network.target remote-fs.target nss-lookup.target
Documentation=man:httpd@.service(8)

[Service]
Type=notify
Environment=LANG=C
Environment=HTTPD_INSTANCE=%i
ExecStartPre=/bin/mkdir -m 710 -p /run/httpd/instance-%i
ExecStartPre=/bin/chown root.apache /run/httpd/instance-%i
ExecStart=/usr/sbin/httpd $OPTIONS -DFOREGROUND -f conf/%i.conf
ExecReload=/usr/sbin/httpd $OPTIONS -k graceful -f conf/%i.conf
# Send SIGWINCH for graceful stop
KillSignal=SIGWINCH
KillMode=mixed
PrivateTmp=true

[Install]
WantedBy=multi-user.target
```
</details>

То есть все что требуется для запуска дополнительного инстанса апача
с кастомными настройками, это подложить файл конфига в каталог
`/etc/httpd/conf/`, например `qwerty.conf`

и запустить его соответствующей командой (`systemctl start httpd@qwerty`)

Для примера на стенде запускается инстанст с дефолтным конфигом на порту 80 и qwerty на порту 8080


## systemctl status

<details>
<summary>выхлоп команды для сервисов</summary>

```
[root@otus vagrant]# systemctl status httpd httpd@qwerty monitor monitor.timer spawn-fcgi
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
   Active: active (running) since Sun 2020-09-13 17:33:49 UTC; 9min ago
     Docs: man:httpd.service(8)
 Main PID: 10273 (httpd)
   Status: "Running, listening on: port 80"
    Tasks: 213 (limit: 6107)
   Memory: 32.4M
   CGroup: /system.slice/httpd.service
           ├─10273 /usr/sbin/httpd -DFOREGROUND
           ├─10277 /usr/sbin/httpd -DFOREGROUND
           ├─10278 /usr/sbin/httpd -DFOREGROUND
           ├─10279 /usr/sbin/httpd -DFOREGROUND
           └─10280 /usr/sbin/httpd -DFOREGROUND

Sep 13 17:33:49 otus systemd[1]: Starting The Apache HTTP Server...
Sep 13 17:33:49 otus httpd[10273]: AH00558: httpd: Could not reliably determine the server's fully>
Sep 13 17:33:49 otus systemd[1]: Started The Apache HTTP Server.
Sep 13 17:33:49 otus httpd[10273]: Server configured, listening on: port 80

● httpd@qwerty.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd@.service; enabled; vendor preset: disabled)
   Active: active (running) since Sun 2020-09-13 17:33:50 UTC; 9min ago
     Docs: man:httpd@.service(8)
 Main PID: 10798 (httpd)
   Status: "Running, listening on: port 8080"
    Tasks: 213 (limit: 6107)
   Memory: 24.1M
   CGroup: /system.slice/system-httpd.slice/httpd@qwerty.service
           ├─10798 /usr/sbin/httpd -DFOREGROUND -f conf/qwerty.conf
           ├─10801 /usr/sbin/httpd -DFOREGROUND -f conf/qwerty.conf
           ├─10802 /usr/sbin/httpd -DFOREGROUND -f conf/qwerty.conf
           ├─10803 /usr/sbin/httpd -DFOREGROUND -f conf/qwerty.conf
           └─10806 /usr/sbin/httpd -DFOREGROUND -f conf/qwerty.conf

Sep 13 17:33:50 otus systemd[1]: Starting The Apache HTTP Server...
Sep 13 17:33:50 otus httpd[10798]: AH00558: httpd: Could not reliably determine the server's fully>
Sep 13 17:33:50 otus httpd[10798]: Server configured, listening on: port 8080
Sep 13 17:33:50 otus systemd[1]: Started The Apache HTTP Server.

● monitor.service - Check if log contains pattern
   Loaded: loaded (/etc/systemd/system/monitor.service; static; vendor preset: disabled)
   Active: inactive (dead) since Sun 2020-09-13 17:43:29 UTC; 11s ago
  Process: 25775 ExecStart=/usr/bin/monitor (code=exited, status=0/SUCCESS)
 Main PID: 25775 (code=exited, status=0/SUCCESS)

Sep 13 17:43:29 otus systemd[1]: Starting Check if log contains pattern...
Sep 13 17:43:29 otus monitor[25775]: ansible found in /var/log/messages
Sep 13 17:43:29 otus systemd[1]: Started Check if log contains pattern.

● monitor.timer - Run monitor each 30 seconds
   Loaded: loaded (/etc/systemd/system/monitor.timer; enabled; vendor preset: disabled)
   Active: active (waiting) since Sun 2020-09-13 17:33:48 UTC; 9min ago
  Trigger: Sun 2020-09-13 17:43:59 UTC; 18s left

Sep 13 17:33:48 otus systemd[1]: Started Run monitor each 30 seconds.

● spawn-fcgi.service - Spawn FastCGI scripts to be used by web servers
   Loaded: loaded (/etc/systemd/system/spawn-fcgi.service; enabled; vendor preset: disabled)
   Active: active (running) since Sun 2020-09-13 17:33:50 UTC; 9min ago
     Docs: man:spawn-fcgi(1)
 Main PID: 11320 (php-cgi)
    Tasks: 5 (limit: 6107)
   Memory: 7.0M
   CGroup: /system.slice/spawn-fcgi.service
           ├─11320 /usr/bin/php-cgi
           ├─11321 /usr/bin/php-cgi
           ├─11322 /usr/bin/php-cgi
           ├─11323 /usr/bin/php-cgi
           └─11324 /usr/bin/php-cgi

Sep 13 17:33:50 otus systemd[1]: Starting Spawn FastCGI scripts to be used by web servers...
Sep 13 17:33:50 otus spawn-fcgi[11316]: spawn-fcgi: child spawned successfully: PID: 11320
Sep 13 17:33:50 otus systemd[1]: Started Spawn FastCGI scripts to be used by web servers.
```

</details>
