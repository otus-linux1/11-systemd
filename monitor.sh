#! /usr/bin/env bash

err() {
    echo "set variables KEYWORD and PATH in /etc/sysconfig/monitor"
    exit 1
}

if [[ -z $KEYWORD || -z $FILEPATH ]]; then
    err
fi

grep $KEYWORD $FILEPATH &>/dev/null
ret=$?
if [[ $ret -ne 0 ]]; then
    echo "${KEYWORD} not found in ${FILEPATH}"
else
    echo "${KEYWORD} found in ${FILEPATH}"
fi
